---
title: 'UCL Praktik Rapport'
subtitle: 'Kjær-Data 01-08-2020 - 31-10-2020'
authors: ['Tim Nikolajsen']
main_author: ['']
date: \today
#institution: UCL Student Sikkerheds Audit
email: 'timx1571@edu.ucl.dk'
left-header: \today
right-header: Kjær-Data Praktik
skip-toc: false
toc-depth: 1
skip-lof: true
---

<!-- Praktikrapporten må max være 10 normalsider foruden bilag.
En normalside er 2.400 tegn inkl. mellemrum og fodnoter. Forside, indholdsfortegnelse, litteraturliste samt bilag
tæller ikke med heri. Bilag er uden for bedømmelse. -->


# Forord

Denne rapport ville ikke være mulig at få skrevet, hvis Kjær-Data ikke havde ladet mig være hos dem i min praktikperiode. De har givet gode rammer for at jeg kunne lave arbejde, som er relevant for uddannelse uden at være krævende eller begrænsende for min udførsel af opgaver.

\newpage
# Indledning

Rapporten her omhandler min praktikperiode hos Kjær-Data, som har varet ca. 3 måneder, og vil inkludere en beskrivelse af virksomheden og de konkrete arbejdsopgaver, som jeg selv er blevet stillet/har pålagt mig. Dernæst vil jeg reflektere over læringsmålene, som skal opfyldes for at praktikken er "bestået", og til slut vil der være en lille opsummering om jeg har opfyldt de læringsmål som er stillet, i en konklusion. Af sikkerhedsmæssige årsager inderholder rapporten ingen konkrete informationer om kunder eller tekniske indstillinger.

## Læringsmål

#### **Viden**
Den studerende har viden om og forståelse for:

- Den daglige drift i hele praktikvirksomheden.
- Videnskabsteoretiske metoder

#### **Færdigheder**
Den studerende kan:

- Anvende alsidige tekniske og analytiske arbejdsmetoder, der knytter sig til beskæftigelse inden for erhvervet
- Vurdere praksisnære problemstillinger og opstilling af løsningsmuligheder
- Formidle praksisnære problemstillinger og begrundede løsningsforslag.

#### **Kompetencer**
Den studerende kan:

- Håndtere udviklingsorienterede praktiske og faglige situationer i forhold til erhvervet
- Tilegne sig ny viden, færdigheder og kompetencer i relation til erhvervet
- Deltage i fagligt og tværfagligt samarbejde med en professionel tilgang.
- Håndtere strukturering og planlægning af daglige arbejdsopgaver i erhvervet 


\newpage
# Beskrivelse af virksomheden

Kjær-Data (KD) er en lille virksomhed på under 20 ansatte, som leverer netværksløsninger, serviceaftaler og hosting til kunder både on- og off-shore samt phishing kampagner. Deres hoved fokus er på det maritime i form at infrastruktur til både skibe og vindmøller i form af ikke kun traditionelt netværk, men også LTE og satellit opkoblinger. Mærkerne og produkter som er det de ansatte er specialiseret i:

- Palo Alto next-gen firewalls
- Cortex XDR Detection and Response App
- Aruba Networks WiFi controllers og access points
- Peplink cellular routers
- Cisco routers og switches

Arbejdet i det maritime introducerer problemer som ikke støder på, når man arbejder med normalt netværks arbejde, hvilket gør at man skal tænke mere "out-of-the-box" end normalt.

Udover netværks løsningerne, så er KD også begyndt at kigge på at yde IT Sikkerheds services some Audits, hvilket er en ny retning for virksomheden og er noget som er under opbygning, og som er en del af praktik arbejdet.

\newpage
# Beskrivelse af konkrete arbejdsopgaver

I praktikperioden var der ikke mange opgaver, men opgaverne har i stedet været i den større ende af skalaen. Praktikkens opgaver beskrives herunder i kronologisk rækkefølge og inlkuderer vage referencer til dokumenter, hvor alle kilder kan findes i bilaget.

## High Level Sikkerhedsvurdering

Den først opgave bestod sig i at der skulle laves en meget high level sikkerhedsvurdering af KD. Fremgangsmåden bestod sig i at der skulle defineres spørgsmål, som blev brugt til at få indsigt i KD's IT Sikkerhed. Spørgsmålene var lavet ud fra en kombination af en ISAE 3402 lavet på [Unit-IT](https://unit-it.dk/media/1361/unit-it-isae_3402-2018-dk.pdf) og [ISO 27002](https://www.iso.org/standard/54533.html) standarden. Spørgerunden foregik i møder sammen med chefen og en kollega som var ansvarlig for politikkerne for KD.  Med en high level vurdering, menes der at der kun kigges meget overfladisk på KD's IT sikkerhed, med spørgsmål som: "Har i en IT sikkerhedspolitik?" og "Har i drift procedurer og ansvarsområder?", for at nævne et par eksempler.\
Resultatet af vurderingen viste at KD ikke havde en IT sikkerhedspolitik, og disaster recovery planen (DRP), som man troede lå på Microsoft SharePoint,  viste sig ikke at være til at finde, så den skulle laves igen.

## IT Sikkerhedspolitik

IT sikkerhedspolitik opgaven var en hård nød at knække da frameworket, som blev inspireret af [Morsø kommune's](https://www.mors.dk/media/3266/itplussikkerhedspolitik.pdf) IT sikkerhedspolitik, ikke var let at få konverteret til at passe til KD grundet den store branche forskel. Meget af arbejdet er gået med at snakke med ansvarlige for de forskellige områder om, hvordan virksomhedens uskrevne politikker fungere i virkeligheden og få informationerne skrevet ned på en sådan måde at politikkerne ikke modarbejdet forretningsmodellen. Selv for en lille virksomhed som KD, er en IT sikkerheds politik ikke nogen lille opgave. Den første draft, som blev sendt til chefen, for at få feedback, var på 16 sider, uden at være i nærheden af at være færdig.

## Disaster Recovery Plan

Uden en DRP blev det min opgave, som IT Security representant, at få den udarbejdet så de ansatte kender proceduren, hvis katastrofen er ude. DRP'er er utroligt store dokumenter, som er utrolig svære at få udarbejdet fra bunden af, hvis man ikke har noget erfaring med det. Derfor er der draget inspiration til at komme i gang med et gratis template fra [microfocus.com](https://www.microfocus.com/media/unspecified/disaster_recovery_planning_template_revised.pdf), som gav et rigtig godt springbræt til at komme i gang. Meget af arbejdet på DRP'en er gået med at få templatet oversat til Dansk og rettet den ind til at passe med KD's forretningsmodel. DRP'en nåede ikke at blive færdig før da der kom en kunde opgave, som havde forrang for interne opgaver. Der bliver dog arbejdet videre på den, som en del af bachelor projektet.

\newpage
## Password Manager

Mens jeg arbejdede på DRP'en og sikkerhedspolitikken, kom der et ønske om at få sat en password manager op. Jeg bragte det op til et møde med chefen og han syntes også det var en god idé. Jeg begyndte at researche, hvilken manager vi skulle bruge, når jeg havde brug for at lave noget andet end DRP'en og politikken. Valget endte på Bitwarden grundet behovet for self-hosting og Active Directory integration. Desværre er arbejdet på implementation ikke påbegyndt og er blevet til en del af bachelor projektet.

## Kunde IT Sikkerhedsvurdering

Kunde opgaven i praktikperioden har været at der skulle laves en teknisk sikkerhedsvurdering af en kunde, som KD har lavet noget netværksarbejde for ved en tidligere lejlighed. Efterspørgslen af en teknisk vurdering i en tid med Corona virussen, har gjort at der skulle laves et system, som kunne gøres online spørgerunder i stedet for at kunne mødes face-to-face og få et personligt indblik i deres opsætning og indstillinger. Grundet online naturen af opgaven har det ikke været super nemt at få informationer ud af kunden, grundet møder osv., men ved at kunne spørge via email har det været lidt nemmere for dem at kunne svare i et tempo, som det har passet dem. Spørgsmålene i vurderingen er baseret på ISO 27002 og [Center for Internet Security's](https://www.cisecurity.org/) (CIS) sikkerhedskontroller med anbefalinger også fra [National Institute of Standards and Technology](https://www.nist.gov/). Og selve vurderingen er lavet ud fra kilderne i [Bibliografien](#bibliografi), som ikke inkluderer ISO 27002 grundet en betalingsmur. Produktet til kunden består sig af en rapport og et PowerPoint, som kunden kan vise til ledelsen, så der kan sættes gang i deres tilrettelser.

\newpage
# Reflektioner over opnåelse af konkrete læringsmål

## Viden

Den daglige drift hos Kjær-Data har været meget stille, både grundet Corona, men August-November lader også til at være stille måneder generelt af årsager jeg ikke kender. Men det betyder ikke at der ikke har været noget at lave for en IT Sikkerheds ansat, da der altid er noget at lave. Hvis ikke for kunder, så er der interne opgaver, som skal løses eller forbedres på. Dette betyder selvfølgelig, at der er et behov for at kunne begrunde disse løsninger. IT sikkerhedsvuderingen for kunden f.eks. fik vurderinger, med begrundelser ved at bruge kilder som CIS, NIST, ISO standarden og tilmed PCI. De er alle store organisationer, som er velkendte inden for IT branchen. Ved at gøre brug af deres kombineret empiriske data er der en autoritativ kilde, som er brugt til begrundelse for vurderingen til kunden. At have sådanne kilder har gjort at jeg ikke selv behøvede at bruge store mængder ressourcer på at forske i noget, som andre har gjort allerede, hvilket har gjort at jeg hurtigere, har kunnet lave min vurderingsopgave på et godt niveau til kunden.

## Færdigheder

Password manager opgaven blev igangsat i praktikperioden, men bliver først implementeret under bachelor projektet. Men under praktikken lavede jeg en del research for at finde en række forkellige produkter, som jeg tage op med chefen. Jeg kiggede, i første omgang, efter en del forskellige for at få en ide om landskabet indenfor området. Så da jeg bragte dem jeg fandt på banen til chefen, havde han haft tid til at tænke over, hvad han godt ville have i løsningen. Dette ændrede listen over password managers fra en 7-8 stk. ned til 2, hvor den ene (KeePass) er mere egnet til en enkelt bruger, hvor den anden (Bitwarden) er mere egnet til virksomheder. Jeg valgte at bringe Bitwarden op igen, og mine sikkerheds argumenter for at det skulle være den var chefen enig med mig i. At kunne argumentere for at en løsning er bedre end en anden, uden bare at sige "Det siger den her YouTube video" er både en fed følelse og er godt for virksomhedens sikkerhed.

## Kompetencer

Opgaven for kunden viste sig at have nogle områder, som jeg ikke kendte så meget til. Dette betød at jeg var nødt til at læse op på for at have den mindste chance for at kunne vurdere på. Det har primært været cloud sikkerhed, hvor udbyderne heldigvis har god dokumentation, så når jeg fandt de rette dokumenter, var det bare et spørgsmål om at skimme/læse det igennem. Der var også nogle dokumenter for standarder (RFC'er), som jeg var nødt til at finde, for at få den nødvendige tekniske viden inden for en protokol for at kunne vurdere på den.

Planlægning af arbejde ligger i øjeblikket (uden for bachelor projektet) i, at når jeg støder på procedurer som skal skrives ned (dokumenter som mangler osv.), så bliver de skrevet ind i et notationsprogram, som jeg altid har åbent, så jeg forhåbentlig aldrig sidder uden arbejdesopgaver.

\newpage
# Konklusion
I praktikken har der været en håndfuld opgaver, som har været store og gjort, at opfyldningen af læringsmålene ikke nødvendigvis er perfekte, men at der er brugt og dannet viden, som skabt færdigheder, og har udvidet kompetencerne, som en kommende IT Sikkerhedsuddannet.


\newpage
# Bilag:
Praktik Logbogen: [https://gitlab.com/Azarion/bachelor-internship/-/blob/master/logbook.md](https://gitlab.com/Azarion/bachelor-internship/-/blob/master/logbook.md)\
High Level Spørgeskema (Kræver download): [https://gitlab.com/Azarion/bachelor-internship/-/blob/master/docs/sp%C3%B8rgsm%C3%A5l_og_svar.xlsx](https://gitlab.com/Azarion/bachelor-internship/-/blob/master/docs/sp%C3%B8rgsm%C3%A5l_og_svar.xlsx)

## Bibliografi

[1]Amazon Web Services, Inc, “Security & Compliance Quick Reference Guide.” Amazon.com, Inc., 2018, Accessed: Aug. 09, 2020. [Online]. Available: [https://d1.awsstatic.com/whitepapers/compliance/AWS_Compliance_Quick_Reference.pdf](https://d1.awsstatic.com/whitepapers/compliance/AWS_Compliance_Quick_Reference.pdf).\
[2]Amazon Web Services, Inc, “Amazon Web Services: Overview of Security Processes.” Amazon.com, Inc., Mar. 2020, Accessed: Jul. 09, 2020. [Online]. Available: [https://d0.awsstatic.com/whitepapers/aws-security-whitepaper.pdf](https://d0.awsstatic.com/whitepapers/aws-security-whitepaper.pdf).\
[3]Amazon Web Services, Inc, “Best practices for working with AWS Lambda functions,” N/A. [https://docs.aws.amazon.com/lambda/latest/dg/best-practices.html](https://docs.aws.amazon.com/lambda/latest/dg/best-practices.html) (accessed Sep. 15, 2020).\
[4]Amazon Web Services, Inc, “Encrypting Data at Rest,” N/A. [https://docs.aws.amazon.com/efs/latest/ug/encryption-at-rest.html](https://docs.aws.amazon.com/efs/latest/ug/encryption-at-rest.html) (accessed Aug. 09, 2020).\
[5]Amazon Web Services, Inc, “Encrypting Data in Transit,” N/A. [https://docs.aws.amazon.com/efs/latest/ug/encryption-in-transit.html](https://docs.aws.amazon.com/efs/latest/ug/encryption-in-transit.html) (accessed Aug. 09, 2020).\
[6]Amazon Web Services, Inc, “Security Best Practices for Amazon S3,” N/A. [https://docs.aws.amazon.com/AmazonS3/latest/dev/security-best-practices.html](https://docs.aws.amazon.com/AmazonS3/latest/dev/security-best-practices.html) (accessed Sep. 22, 2020).\
[7]Amazon Web Services, Inc, “Security best practices for Elastic Beanstalk,” N/A. [https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/security-best-practices.html](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/security-best-practices.html) (accessed Sep. 15, 2020).\
[8]Center for Internet Security, “CIS Controls Cloud Companion Guide.” Center for Internet Security, N/A, Accessed: Aug. 28, 2020. [Online]. Available: [https://cdn2.hubspot.net/hubfs/2101505/CIS%20Controls%20Cloud%20Companion%20Guide.pdf](https://cdn2.hubspot.net/hubfs/2101505/CIS%20Controls%20Cloud%20Companion%20Guide.pdf).\
[9]Google, LLC, “Encryption in Transit in Google Cloud,” Dec. 2017. [https://cloud.google.com/security/encryption-in-transit](https://cloud.google.com/security/encryption-in-transit) (accessed Aug. 09, 2020).\
[10]Google, LLC, “Google Cloud Security Whitepapers.” Google, LLC, Mar. 2018, Accessed: Jul. 09, 2020. [Online]. Available: [https://services.google.com/fh/files/misc/security_whitepapers_march2018.pdf](https://services.google.com/fh/files/misc/security_whitepapers_march2018.pdf).\
[11]Google, LLC, “Google security whitepaper.” Google, LLC, Jan. 2019, Accessed: Jul. 09, 2020. [Online]. Available: [https://services.google.com/fh/files/misc/google_security_wp.pdf](https://services.google.com/fh/files/misc/google_security_wp.pdf).\
[12]Google, LLC, “Encryption at rest in Google Cloud,” Jul. 2020. [https://cloud.google.com/security/encryption-at-rest/default-encryption](https://cloud.google.com/security/encryption-at-rest/default-encryption) (accessed Aug. 09, 2020).\
[13]Google, LLC, “Compliance resource center,” N/A. [https://cloud.google.com/security/compliance](https://cloud.google.com/security/compliance) (accessed Jul. 09, 2020).\
[14]Ian Maddox and Kyle Moschetto, “Modern password security for users.” Google, LLC, N/A, Accessed: Aug. 09, 2020. [Online]. Available: [https://cloud.google.com/solutions/modern-password-security-for-users.pdf](https://cloud.google.com/solutions/modern-password-security-for-users.pdf).\
[15]D. Harkins and D. Carrel, “The Internet Key Exchange (IKE),” Nov. 1998. [https://tools.ietf.org/html/rfc2409](https://tools.ietf.org/html/rfc2409) (accessed Oct. 01, 2020).\
[16]D. Harkins and D. Carrel, “The Internet Key Exchange (IKE),” Nov. 1998. [https://tools.ietf.org/html/rfc2409](https://tools.ietf.org/html/rfc2409) (accessed Oct. 20, 2020).\
[17]C. Kaufman, P. Hoffman, Y. Nir, and P. Eronen, “Internet Key Exchange Protocol Version 2 (IKEv2),” Sep. 2010. [https://tools.ietf.org/html/rfc5996](https://tools.ietf.org/html/rfc5996) (accessed Oct. 20, 2020).\
[18]C. Kaufman, P. Hoffman, Y. Nir, P. Eronen, and T. Kivinen, “Internet Key Exchange Protocol Version 2 (IKEv2),” Oct. 2014. [https://tools.ietf.org/html/rfc7296](https://tools.ietf.org/html/rfc7296) (accessed Oct. 02, 2020).\
[19]T. Kivinen and M. Kojo, “More Modular Exponential (MODP) Diffie-Hellman groups for Internet Key Exchange (IKE),” May 2003. [https://tools.ietf.org/html/rfc3526](https://tools.ietf.org/html/rfc3526).\
[20]M. Lepinski and S. Kent, “Additional Diffie-Hellman Groups for Use with IETF Standards,” Jan. 2008. [https://tools.ietf.org/html/rfc5114](https://tools.ietf.org/html/rfc5114) (accessed Oct. 20, 2020).\
[21]Y. Nir and S. Josefsson, “Curve25519 and Curve448 for the      Internet Key Exchange Protocol Version 2 (IKEv2) Key Agreement,” Dec. 2016. [https://tools.ietf.org/html/rfc8031](https://tools.ietf.org/html/rfc8031) (accessed Oct. 20, 2020).\
[22]Microsoft, “Best Practices for Securing Active Directory,” May 31, 2017. [https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/security-best-practices/best-practices-for-securing-active-directory](https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/security-best-practices/best-practices-for-securing-active-directory) (accessed Sep. 28, 2020).\
[23]Microsoft, “Azure network security overview,” Oct. 29, 2018. [https://docs.microsoft.com/en-us/azure/security/fundamentals/network-overview](https://docs.microsoft.com/en-us/azure/security/fundamentals/network-overview) (accessed Jul. 09, 2020).\
[24]Microsoft, “Storage, data, and encryption,” Jul. 03, 2019. [https://docs.microsoft.com/en-us/security/compass/storage-data-encryption](https://docs.microsoft.com/en-us/security/compass/storage-data-encryption) (accessed Aug. 09, 2020).\
[25]Microsoft, “Applications and services,” Jul. 08, 2019. [https://docs.microsoft.com/en-us/security/compass/applications-services](https://docs.microsoft.com/en-us/security/compass/applications-services) (accessed Aug. 09, 2020).\
[26]Microsoft, “Identity and access management,” Jul. 09, 2019. [https://docs.microsoft.com/en-us/security/compass/identity](https://docs.microsoft.com/en-us/security/compass/identity) (accessed Aug. 09, 2020).\
[27]Microsoft, “Security operations,” Jul. 09, 2019. [https://docs.microsoft.com/en-us/security/compass/security-operations](https://docs.microsoft.com/en-us/security/compass/security-operations) (accessed Aug. 09, 2020).\
[28]Microsoft, “Encryption,” Aug. 15, 2019. [https://docs.microsoft.com/en-us/microsoft-365/compliance/encryption?view=o365-worldwide](https://docs.microsoft.com/en-us/microsoft-365/compliance/encryption?view=o365-worldwide) (accessed Aug. 28, 2020).\
[29]Microsoft, “What is Cloud App Security,” Microsoft, Aug. 15, 2019. [https://docs.microsoft.com/en-us/cloud-app-security/what-is-cloud-app-security](https://docs.microsoft.com/en-us/cloud-app-security/what-is-cloud-app-security) (accessed Sep. 02, 2020).\
[30]Microsoft, “Azure best practices for network security,” Oct. 02, 2019. [https://docs.microsoft.com/en-us/azure/security/fundamentals/network-best-practices](https://docs.microsoft.com/en-us/azure/security/fundamentals/network-best-practices) (accessed Jul. 09, 2020).\
[31]Microsoft, “Cloud App Security best practices,” Oct. 24, 2019. [https://docs.microsoft.com/en-us/cloud-app-security/best-practices](https://docs.microsoft.com/en-us/cloud-app-security/best-practices) (accessed Aug. 28, 2020).\
[32]Microsoft, “BitLocker and Distributed Key Manager (DKM) for Encryption,” May 05, 2020. [https://docs.microsoft.com/en-us/microsoft-365/compliance/office-365-bitlocker-and-distributed-key-manager-for-encryption?view=o365-worldwide](https://docs.microsoft.com/en-us/microsoft-365/compliance/office-365-bitlocker-and-distributed-key-manager-for-encryption?view=o365-worldwide) (accessed Aug. 28, 2020).\
[33]Microsoft, “Security recommendations for Blob storage,” Jun. 17, 2020. [https://docs.microsoft.com/en-us/azure/storage/blobs/security-recommendations](https://docs.microsoft.com/en-us/azure/storage/blobs/security-recommendations) (accessed Jul. 09, 2020).\
[34]Microsoft, “Encryption in Azure,” Jun. 19, 2020. [https://docs.microsoft.com/en-us/microsoft-365/compliance/office-365-azure-encryption?view=o365-worldwide](https://docs.microsoft.com/en-us/microsoft-365/compliance/office-365-azure-encryption?view=o365-worldwide) (accessed Jul. 09, 2020).\
[35]Microsoft, “Azure encryption overview,” Jul. 20, 2020. [https://docs.microsoft.com/en-us/azure/security/fundamentals/encryption-overview](https://docs.microsoft.com/en-us/azure/security/fundamentals/encryption-overview) (accessed Jul. 09, 2020).\
[36]Microsoft, “Microsoft Security Best Practices,” Jul. 22, 2020. [https://docs.microsoft.com/en-us/security/compass/compass](https://docs.microsoft.com/en-us/security/compass/compass) (accessed Aug. 09, 2020).\
[37]Microsoft, “Preparing for TLS 1.2 in Office 365 and Office 365 GCC,” Jul. 23, 2020. [https://docs.microsoft.com/en-us/microsoft-365/compliance/prepare-tls-1.2-in-office-365?view=o365-worldwide](https://docs.microsoft.com/en-us/microsoft-365/compliance/prepare-tls-1.2-in-office-365?view=o365-worldwide) (accessed Aug. 28, 2020).\
[38]Microsoft, “Office 365 Advanced Threat Protection,” Microsoft, Aug. 12, 2020. [https://docs.microsoft.com/en-us/microsoft-365/security/office-365-security/office-365-atp?view=o365-worldwide](https://docs.microsoft.com/en-us/microsoft-365/security/office-365-security/office-365-atp?view=o365-worldwide) (accessed Sep. 02, 2020).\
[39]Microsoft, “Azure data encryption at rest,” Aug. 13, 2020. [https://docs.microsoft.com/en-us/azure/security/fundamentals/encryption-atrest](https://docs.microsoft.com/en-us/azure/security/fundamentals/encryption-atrest) (accessed Jul. 09, 2020).\
[40]Microsoft, “Double encryption,” Aug. 13, 2020. [https://docs.microsoft.com/en-us/azure/security/fundamentals/double-encryption](https://docs.microsoft.com/en-us/azure/security/fundamentals/double-encryption) (accessed Aug. 09, 2020).\
[41]Microsoft, “Microsoft Defender Advanced Threat Protection,” Microsoft, Aug. 13, 2020. [https://docs.microsoft.com/en-us/windows/security/threat-protection/microsoft-defender-atp/microsoft-defender-advanced-threat-protection](https://docs.microsoft.com/en-us/windows/security/threat-protection/microsoft-defender-atp/microsoft-defender-advanced-threat-protection) (accessed Aug. 28, 2020).\
[42]E. Barker, “Guideline for Using Cryptographic Standards in the Federal Government: Cryptographic Mechanisms,” Mar. 2020. [https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-175Br1.pdf](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-175Br1.pdf) (accessed Oct. 19, 2020).\
[43]E. Barker, Q. Dang, S. Frankel, K. Scarfone, and P. Wouters, “Guide to IPsec VPNs,” Jun. 2020. [https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-77r1.pdf](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-77r1.pdf) (accessed Oct. 19, 2020).\
[44]E. Barker and A. Roginsky, “Transitioning the Use of Cryptographic Algorithms and Key Lengths,” Mar. 2019. [https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-131Ar2.pdf](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-131Ar2.pdf) (accessed Oct. 19, 2020).\
[45]P. A. Grassi, M. E. Garcia, and J. L. Fenton, “Digital Identity Guidelines,” Jun. 2017. [https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-63-3.pdf](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-63-3.pdf) (accessed Oct. 19, 2020).\
[46]Lee Badger, Tim Grance, Robert Patt-Corner, and Jeff Voas, “Cloud Computing Synopsis and Recommendations.” National Institute of Standards and Technology, May 2012, Accessed: Aug. 28, 2020. [Online]. Available: [https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-146.pdf](https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-146.pdf).\
[47]National Institute of Standards and Technology, “Security Requirements For Cryptographic Modules,” May 21, 2001. [https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.140-2.pdf](https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.140-2.pdf) (accessed Aug. 28, 2020).\
[48]National Institute of Standards and Technology, “Block Cipher Techniques,” Jun. 22, 2020. [https://csrc.nist.gov/projects/block-cipher-techniques](https://csrc.nist.gov/projects/block-cipher-techniques) (accessed Oct. 19, 2020).\
[49]Cloud Special Interest Group PCI Security Standards Council, “Information Supplement: PCI SSC Cloud Computing Guidelines.” PCI Consortium, Apr. 2018, Accessed: Aug. 28, 2020. [Online]. Available: [https://www.pcisecuritystandards.org/pdfs/PCI_SSC_Cloud_Guidelines_v3.pdf](https://www.pcisecuritystandards.org/pdfs/PCI_SSC_Cloud_Guidelines_v3.pdf).\
[50]HelpfulTechVids, Installing and Setting Up Bitwarden Password Manager. 2019.\
[51]Microsoft, “Service encryption with Customer Key,” Feb. 05, 2020. [https://docs.microsoft.com/en-us/microsoft-365/compliance/customer-key-overview?view=o365-worldwide](https://docs.microsoft.com/en-us/microsoft-365/compliance/customer-key-overview?view=o365-worldwide) (accessed Aug. 28, 2020).\
