---
title: 'Praktik Logbog'
subtitle: 'Kjær-Data'
authors: ['Tim Nikolajsen']
main_author: ['']
date: \today
#institution: UCL Student Sikkerheds Audit
email: 'timx1571@edu.ucl.dk'
left-header: \today
right-header: Kjær-Data Praktik
skip-toc: false
toc-depth: 1
skip-lof: true
---

# Uge 43

Ugen har stået på at vurdere nogle implementationer hos kunden ved a finde godkendte krypterings algoritmer og opsætningser af VPN, RFC dokumentation for at finde ud af hvad en Diffie-Hellman gruppe er og koble det til min vurdering. Ellers er ugen gået med at skrive rapporten til kunden samt lave en præsentation, som de kan vise til ledelsen.

## Reflektion

Det er fedt at have en citation manager som Zotero til at holde styr på og lave bibliografi lister, så jeg ikke selv skal holde styr på det i hovedet. Det gør det hele lettere for mig selv at finde en gammel kilde, frem for at jeg skal huske, hvad jeg søgt i sin tid. Det kræver dog lidt tilvending før det bliver en integreret del af mit work-flow.

## Bibliografi
[1]E. Barker, “Guideline for Using Cryptographic Standards in the Federal Government: Cryptographic Mechanisms,” Mar. 2020. [https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-175Br1.pdf](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-175Br1.pdf) (accessed Oct. 19, 2020).\
[2]D. Harkins and D. Carrel, “The Internet Key Exchange (IKE),” Nov. 1998. [https://tools.ietf.org/html/rfc2409](https://tools.ietf.org/html/rfc2409) (accessed Oct. 20, 2020).\
[3]National Institute of Standards and Technology, “Block Cipher Techniques,” Jun. 22, 2020. [https://csrc.nist.gov/projects/block-cipher-techniques](https://csrc.nist.gov/projects/block-cipher-techniques) (accessed Oct. 19, 2020).\
[4]E. Barker and A. Roginsky, “Transitioning the Use of Cryptographic Algorithms and Key Lengths,” Mar. 2019. [https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-131Ar2.pdf](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-131Ar2.pdf) (accessed Oct. 19, 2020).\
[5]E. Barker, Q. Dang, S. Frankel, K. Scarfone, and P. Wouters, “Guide to IPsec VPNs,” Jun. 2020. [https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-77r1.pdf](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-77r1.pdf) (accessed Oct. 19, 2020).\
[6]P. A. Grassi, M. E. Garcia, and J. L. Fenton, “Digital Identity Guidelines,” Jun. 2017. [https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-63-3.pdf](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-63-3.pdf) (accessed Oct. 19, 2020).\
[7]C. Kaufman, P. Hoffman, Y. Nir, and P. Eronen, “Internet Key Exchange Protocol Version 2 (IKEv2),” Sep. 2010. [https://tools.ietf.org/html/rfc5996](https://tools.ietf.org/html/rfc5996).\
[8]T. Kivinen and M. Kojo, “More Modular Exponential (MODP) Diffie-Hellman groups for Internet Key Exchange (IKE),” May 2003. [https://tools.ietf.org/html/rfc3526](https://tools.ietf.org/html/rfc3526).\
[9]M. Lepinski and S. Kent, “Additional Diffie-Hellman Groups for Use with IETF Standards,” Jan. 2008. [https://tools.ietf.org/html/rfc5114](https://tools.ietf.org/html/rfc5114) (accessed Oct. 20, 2020).\
[10]Y. Nir and S. Josefsson, “Curve25519 and Curve448 for the      Internet Key Exchange Protocol Version 2 (IKEv2) Key Agreement,” Dec. 2016. [https://tools.ietf.org/html/rfc8031](https://tools.ietf.org/html/rfc8031) (accessed Oct. 20, 2020).


\newpage
# Uge 42

Havde i denne uge et sidste møde med kunden for at få nogle sidste informationer til vurderingen, som jeg ellers har arbejdet med resten af ugen.

## Reflektion

Det er frustrerende at være nødt til at holde møder, hvor jeg er bliver nødt til at spørge om de samme emner, som jeg har spurgt om tidligere, fordi jeg bliver kastet ud i en opgave, hvor jeg godt ville have haft tid til at lave systemet til vurderingen. Så kunne jeg gå fra et emne til et andet uden at skulle frem og tilbage hele tiden og ikke lyde som om jeg bare finder nye ting at spørge ind til. Mit system skriger 'ad hoc', når man kigger på det grundet udkastet..

## Bibliografi

Ingen nye kilder.

\newpage
# Uge 41

Der har været meget sikkerhedsvurdering i denne uge og en smule planlægning af bachelor projektet, da jeg løb tør for informationer fra kunden. Har et møde med dem igen på Torsdag, hvilket vil blive det sidste møde, hvis jeg skal være færdig inden bachelor projektet.

## Reflektion

Jeg har meget at få snakket med kunden om til det næste møde for at få så mange informationer, for at lave vurderingen så god som muligt inden afsluttende rapportering. Det er stressende, når dem man skal have informationer har for travlt til at snakke med en.  Heldigvis kan man finpudse afrapporterings materialet (Word, Powerpoint), så det er hurtigere at få rapporten ud.

## Bibliografi

Ingen nye kilder.

\newpage
# Uge 40

Der har været rigtig meget reel vurdering. Der kom svar på mit request, som jeg har lavede vudering på, samt sendt endu et request, som jeg ikke har fået svar på endnu. Måtte læse om IKE tunnels for at finde ud af forskellen mellem version 1 og version 2 og hvordan de skulle vurderes.

## Reflektion

Den her form for udførsel af en sikkerhedsvurdering, hvor man vuderdere fra spørgsmål, er ikke min foretrukne måde at lave en vurdering på, da jeg er underlagt de svar, som jeg får af kunden. Men med Corona på banen er er der ikke noget at gøre ved det. Jeg ville foretrække at kunne få lov til at kigge i deres systemer selv, men den fremgangsmåde er nok også mere egnet til en ISO certificering eller GDPR vurdering.

## Bibliografi

[1]Microsoft, “Best Practices for Securing Active Directory,” May 31, 2017. [https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/security-best-practices/best-practices-for-securing-active-directory](https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/plan/security-best-practices/best-practices-for-securing-active-directory) (accessed Sep. 28, 2020).\
[2]C. Kaufman, P. Hoffman, Y. Nir, P. Eronen, and T. Kivinen, “Internet Key Exchange Protocol Version 2 (IKEv2),” Oct. 2014. [https://tools.ietf.org/html/rfc7296](https://tools.ietf.org/html/rfc7296) (accessed Oct. 02, 2020).\
[3]D. Harkins and D. Carrel, “The Internet Key Exchange (IKE),” Nov. 1998. [https://tools.ietf.org/html/rfc2409](https://tools.ietf.org/html/rfc2409) (accessed Oct. 01, 2020).

\newpage
# Uge 39

Ugen har været ret stille. Jeg sendte et informations request til kunden og har hele ugen ventet svar. I mellemtiden har jeg arbejdet på at gøre mit vurderings system bedre og mere fyldestgørende. Det inkluderer at få gennemgået ISO, CIS og NIST kontrollerne for at finde ting jeg har enten glemt eller ikke kendte til.

## Reflektion

Ikke så meget at reflektere om i denne uge. Det er godt at have noget at tage sig til, når man venter på andre personer. Det er utrolig nemt at glemme småting når man laver et sikkerhedsvurdering. Både når man kigger på ét system og når man laver et system, som kan bruges generelt.

## Bibliografi

[1]Amazon Web Services, Inc, “Security Best Practices for Amazon S3,” N/A. [https://docs.aws.amazon.com/AmazonS3/latest/dev/security-best-practices.html](https://docs.aws.amazon.com/AmazonS3/latest/dev/security-best-practices.html) (accessed Sep. 22, 2020).

\newpage
# Uge 38

I denne uge havde jeg et møde med den tidligere ansatte for at få noget feedback på rapporten inden den skulle afleveres. Det viste sig at den opgave jeg havde brugt et par uger på ikke var det, som var forventet af kunden og beskrevet af Henrik. Det viste sig at det var en reel dybdegående sikkerhedsvurdering. Så inden Mandag blev der arbejdet intenst på at lave en system, så jeg kan holde styr på hvad jeg skal spørge efter og hvad hvad jeg har fået (Ikke engang helt færdig). Mødet med kunden om Fredagen gik bedre end forventet og viste sig at jeg var den eneste der ikke vidste at det skulle være så dybdegående. Fredagen var også en team-building dag, så der var en lille tur til Funky Monkey Park.

## Reflektion

Det er super frustrerende, når man laver en opgave som viser sig ikke at være det man har brugt tid på og tror man er færdig med. Spørger helt sikkert mere ind til opgaver i fremtiden for at være endnu mere sikker. Jeg havde endda informeret Henrik om at jeg ikke var klar på at lave så stor en opgave, da jeg ikke havde et system til at holde styr på, områder/emner og svar.

## Bibliografi

[1]Amazon Web Services, Inc, “Best practices for working with AWS Lambda functions,” N/A. [https://docs.aws.amazon.com/lambda/latest/dg/best-practices.html](https://docs.aws.amazon.com/lambda/latest/dg/best-practices.html) (accessed Sep. 15, 2020).\
[2]Amazon Web Services, Inc, “Security best practices for Elastic Beanstalk,” N/A. [https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/security-best-practices.html](https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/security-best-practices.html) (accessed Sep. 15, 2020).

\newpage
# Uge 37

Ugens opgaver har stået på at få færdig lavet rapporten og forsøge at få fat aftalt et møde med en tidligere ansat hos Kjaer-Data, som nu arbejder hos kunden, for at få lidt feedback inden rapporten bliver afleveret til deres CISO. Hans kalender er fyldt op, så jeg er nødt til lige at hører ham ad om han ar tid inden afleveringen inden Fredag næste uge. Mens jeg forsøgte at få fat i ham/vente på at han skriver tilbage har jeg arbejdet på dokumentationen til Bitwarden, når det er muligt at komme videre med det projekt (Den ansvarlige for POC miljøet har været utilgængelig i denne uge). Har også skrevet mig op (sammen med et par kollegaer) til Palo Alto's 10-week Cortex XDR certificering (PSE-P), som jeg bruger ca. 2 timer af min fritid på om ugen de næste par uger.

## Reflektion
Der har været en del læsning efter CISO'en fra kunden fortalte at de har fingrene i alle de største cloud udbydere, så det med at finde de rigtige artikler og dokumentationer tog både lang tid at finde og skimme igennem for at finde sikkerhedsinformationerne, som jeg skal bruge. Godt at links'ne er gemt I Zotero, så det er super nemt at finde i fremtiden. Det er også typisk at man aldrig kan få fat i den person man skal bruge, når skal bruge dem. Men i det mindste er der rigeligt med interne projekter, som jeg kan arbejde på. Det er fedt at lære om Palo Alto's "Next-Gen" produkter, men det er langt mere salgs træning nogen af os havde forventet.. Jeg tror ikke jeg havde sagt ja, hvis jeg havde vidst der var så meget fokus på salg.

## Bibliografi
[1]Amazon Web Services, Inc, “Security & Compliance Quick Reference Guide.” Amazon.com, Inc., 2018, Accessed: Aug. 09, 2020. [Online]. Available: [https://d1.awsstatic.com/whitepapers/compliance/AWS_Compliance_Quick_Reference.pdf](https://d1.awsstatic.com/whitepapers/compliance/AWS_Compliance_Quick_Reference.pdf).\
[2]Amazon Web Services, Inc, “Amazon Web Services: Overview of Security Processes.” Amazon.com, Inc., Mar. 2020, Accessed: Jul. 09, 2020. [Online]. Available: [https://d0.awsstatic.com/whitepapers/aws-security-whitepaper.pdf](https://d0.awsstatic.com/whitepapers/aws-security-whitepaper.pdf).\
[3]Amazon Web Services, Inc, “Encrypting Data at Rest,” N/A. [https://docs.aws.amazon.com/efs/latest/ug/encryption-at-rest.html](https://docs.aws.amazon.com/efs/latest/ug/encryption-at-rest.html) (accessed Aug. 09, 2020).\
[4]Amazon Web Services, Inc, “Encrypting Data in Transit,” N/A. [https://docs.aws.amazon.com/efs/latest/ug/encryption-in-transit.html](https://docs.aws.amazon.com/efs/latest/ug/encryption-in-transit.html) (accessed Aug. 09, 2020).\
[5]Bitwarden, “Configuring directory sync with Active Directory or other LDAP servers,” N/A. [https://bitwarden.com/help/article/ldap-directory/](https://bitwarden.com/help/article/ldap-directory/) (accessed Sep. 11, 2020).\
[6]Bitwarden, “Configuring directory sync with Azure Active Directory,” N/A. [https://bitwarden.com/help/article/azure-active-directory/](https://bitwarden.com/help/article/azure-active-directory/) (accessed Sep. 11, 2020).\
[7]Bitwarden, “Installing and deploying,” N/A. [https://bitwarden.com/help/article/install-on-premise/](https://bitwarden.com/help/article/install-on-premise/) (accessed Sep. 11, 2020).\
[8]Bitwarden, “Security FAQs,” N/A. [https://bitwarden.com/help/article/security-faqs/#current-certifications/](https://bitwarden.com/help/article/security-faqs/#current-certifications/) (accessed Sep. 09, 2020).\
[9]Bitwarden, “Syncing users and groups with a directory,” N/A. [https://bitwarden.com/help/article/directory-sync/](https://bitwarden.com/help/article/directory-sync/) (accessed Sep. 11, 2020).\
[10]Bitwarden, “What encryption is being used?,” N/A. [https://bitwarden.com/help/article/what-encryption-is-used/](https://bitwarden.com/help/article/what-encryption-is-used/) (accessed Sep. 09, 2020).\
[11]Bitwarden, “What information is encrypted?,” N/A. [https://bitwarden.com/help/article/what-information-is-encrypted/](https://bitwarden.com/help/article/what-information-is-encrypted/) (accessed Sep. 09, 2020).\
[12]Google, LLC, “Encryption in Transit in Google Cloud,” Dec. 2017. [https://cloud.google.com/security/encryption-in-transit](https://cloud.google.com/security/encryption-in-transit) (accessed Aug. 09, 2020).\
[13]Google, LLC, “Google Cloud Security Whitepapers.” Google, LLC, Mar. 2018, Accessed: Jul. 09, 2020. [Online]. Available: [https://services.google.com/fh/files/misc/security_whitepapers_march2018.pdf](https://services.google.com/fh/files/misc/security_whitepapers_march2018.pdf).\
[14]Google, LLC, “Google security whitepaper.” Google, LLC, Jan. 2019, Accessed: Jul. 09, 2020. [Online]. Available: [https://services.google.com/fh/files/misc/google_security_wp.pdf](https://services.google.com/fh/files/misc/google_security_wp.pdf).\
[15]Google, LLC, “Encryption at rest in Google Cloud,” Jul. 2020. [https://cloud.google.com/security/encryption-at-rest/default-encryption](https://cloud.google.com/security/encryption-at-rest/default-encryption) (accessed Aug. 09, 2020).\
[16]Google, LLC, “Compliance resource center,” N/A. [https://cloud.google.com/security/compliance](https://cloud.google.com/security/compliance) (accessed Jul. 09, 2020).\
[17]HelpfulTechVids, Installing and Setting Up Bitwarden Password Manager. 2019.\
[18]Microsoft, “Azure network security overview,” Oct. 29, 2018. [https://docs.microsoft.com/en-us/azure/security/fundamentals/network-overview](https://docs.microsoft.com/en-us/azure/security/fundamentals/network-overview) (accessed Jul. 09, 2020).\
[19]Microsoft, “Storage, data, and encryption,” Jul. 03, 2019. [https://docs.microsoft.com/en-us/security/compass/storage-data-encryption](https://docs.microsoft.com/en-us/security/compass/storage-data-encryption) (accessed Aug. 09, 2020).\
[20]Microsoft, “Applications and services,” Jul. 08, 2019. [https://docs.microsoft.com/en-us/security/compass/applications-services](https://docs.microsoft.com/en-us/security/compass/applications-services) (accessed Aug. 09, 2020).\
[21]Microsoft, “Identity and access management,” Jul. 09, 2019. [https://docs.microsoft.com/en-us/security/compass/identity](https://docs.microsoft.com/en-us/security/compass/identity) (accessed Aug. 09, 2020).\
[22]Microsoft, “Security operations,” Jul. 09, 2019. [https://docs.microsoft.com/en-us/security/compass/security-operations](https://docs.microsoft.com/en-us/security/compass/security-operations) (accessed Aug. 09, 2020).\
[23]Microsoft, “Encryption,” Aug. 15, 2019. [https://docs.microsoft.com/en-us/microsoft-365/compliance/encryption?view=o365-worldwide](https://docs.microsoft.com/en-us/microsoft-365/compliance/encryption?view=o365-worldwide) (accessed Aug. 28, 2020).\
[24]Microsoft, “What is Cloud App Security,” Microsoft, Aug. 15, 2019. [https://docs.microsoft.com/en-us/cloud-app-security/what-is-cloud-app-security](https://docs.microsoft.com/en-us/cloud-app-security/what-is-cloud-app-security) (accessed Sep. 02, 2020).\
[25]Microsoft, “Azure best practices for network security,” Oct. 02, 2019. [https://docs.microsoft.com/en-us/azure/security/fundamentals/network-best-practices](https://docs.microsoft.com/en-us/azure/security/fundamentals/network-best-practices) (accessed Jul. 09, 2020).\
[26]Microsoft, “Cloud App Security best practices,” Oct. 24, 2019. [https://docs.microsoft.com/en-us/cloud-app-security/best-practices](https://docs.microsoft.com/en-us/cloud-app-security/best-practices) (accessed Aug. 28, 2020).\
[27]Microsoft, “Service encryption with Customer Key,” Feb. 05, 2020. [https://docs.microsoft.com/en-us/microsoft-365/compliance/customer-key-overview?view=o365-worldwide](https://docs.microsoft.com/en-us/microsoft-365/compliance/customer-key-overview?view=o365-worldwide) (accessed Aug. 28, 2020).\
[28]Microsoft, “BitLocker and Distributed Key Manager (DKM) for Encryption,” May 05, 2020. [https://docs.microsoft.com/en-us/microsoft-365/compliance/office-365-bitlocker-and-distributed-key-manager-for-encryption?view=o365-worldwide](https://docs.microsoft.com/en-us/microsoft-365/compliance/office-365-bitlocker-and-distributed-key-manager-for-encryption?view=o365-worldwide) (accessed Aug. 28, 2020).\
[29]Microsoft, “Security recommendations for Blob storage,” Jun. 17, 2020. [https://docs.microsoft.com/en-us/azure/storage/blobs/security-recommendations](https://docs.microsoft.com/en-us/azure/storage/blobs/security-recommendations) (accessed Jul. 09, 2020).\
[30]Microsoft, “Encryption in Azure,” Jun. 19, 2020. [https://docs.microsoft.com/en-us/microsoft-365/compliance/office-365-azure-encryption?view=o365-worldwide](https://docs.microsoft.com/en-us/microsoft-365/compliance/office-365-azure-encryption?view=o365-worldwide) (accessed Jul. 09, 2020).\
[31]Microsoft, “Azure encryption overview,” Jul. 20, 2020. [https://docs.microsoft.com/en-us/azure/security/fundamentals/encryption-overview](https://docs.microsoft.com/en-us/azure/security/fundamentals/encryption-overview) (accessed Jul. 09, 2020).\
[32]Microsoft, “Microsoft Security Best Practices,” Jul. 22, 2020. [https://docs.microsoft.com/en-us/security/compass/compass](https://docs.microsoft.com/en-us/security/compass/compass) (accessed Aug. 09, 2020).\
[33]Microsoft, “Preparing for TLS 1.2 in Office 365 and Office 365 GCC,” Jul. 23, 2020. [https://docs.microsoft.com/en-us/microsoft-365/compliance/prepare-tls-1.2-in-office-365?view=o365-worldwide](https://docs.microsoft.com/en-us/microsoft-365/compliance/prepare-tls-1.2-in-office-365?view=o365-worldwide) (accessed Aug. 28, 2020).\
[34]Microsoft, “Office 365 Advanced Threat Protection,” Microsoft, Aug. 12, 2020. [https://docs.microsoft.com/en-us/microsoft-365/security/office-365-security/office-365-atp?view=o365-worldwide](https://docs.microsoft.com/en-us/microsoft-365/security/office-365-security/office-365-atp?view=o365-worldwide) (accessed Sep. 02, 2020).\
[35]Microsoft, “Azure data encryption at rest,” Aug. 13, 2020. [https://docs.microsoft.com/en-us/azure/security/fundamentals/encryption-atrest](https://docs.microsoft.com/en-us/azure/security/fundamentals/encryption-atrest) (accessed Jul. 09, 2020).\
[36]Microsoft, “Double encryption,” Aug. 13, 2020. [https://docs.microsoft.com/en-us/azure/security/fundamentals/double-encryption](https://docs.microsoft.com/en-us/azure/security/fundamentals/double-encryption) (accessed Aug. 09, 2020).\
[37]Microsoft, “Microsoft Defender Advanced Threat Protection,” Microsoft, Aug. 13, 2020. [https://docs.microsoft.com/en-us/windows/security/threat-protection/microsoft-defender-atp/microsoft-defender-advanced-threat-protection](https://docs.microsoft.com/en-us/windows/security/threat-protection/microsoft-defender-atp/microsoft-defender-advanced-threat-protection) (accessed Aug. 28, 2020).\


\newpage
# Uge 36

## Mandag/Tirsdag/Onsdag/Torsdag/Fredag
Ikke sket det helt vilde.. Har arbejdet videre med vurderingen af kunden, og fik svar fra en af deres teknikkere om nogle svar jeg skulle have uddybet. Der har været nogle ting, som jeg skulle læse op på, for at jeg kunne lave en sikkerhedsvurdering. Om fredagen havde CISO'en fra kunden tid til at svare på spørgsmål ang. deres cloud setup. Opkaldet skete Fredag morgen, men han skulle til møde, så han ville ringe tilbage, hvilket skete 14:30. I mellemtiden brugte jeg tiden til at forberede at vi kan få Bitwarden på testmiljøet (Dokumentation).

## Reflektion
Teknikkeren fra Kunden svar på "Holder I jer opdateret om hvilke porte er aktive på jeret netværk?" var at det var "en meget gammel tankegang".. Jeg kan godt se hans pointe, når han er Palo Alto sikkerheds certificeret og har nok arbejdet meget med deres App-ID funktionalitet.. Men jeg ved ikke helt hvad jeg skal synes om det endnu.

Det er utroligt svært at få taget telefonen og ringet til kunden for at få noget ekstra information. Da jeg ringede tænkte jeg på om han ville synes jeg var en idiot for ikke at have det med første gang, men der var ingen problemer. JEg er sikker på at det er nemmere næste gang

## Bibliografi
Nogle af de emner jeg læste op på var sikkerheden i Office 365 security, Microsoft Defender ATP, Microsoft Cloud App Security, samt dokumentation fra NIST.

[1]Microsoft, “BitLocker and Distributed Key Manager (DKM) for Encryption,” May 05, 2020.  
[https://docs.microsoft.com/en-us/microsoft-365/compliance/office-365-bitlocker-and-distributed-key-manager-for-encryption?view=o365-worldwide](https://docs.microsoft.com/en-us/microsoft-365/compliance/office-365-bitlocker-and-distributed-key-manager-for-encryption?view=o365-worldwide) (accessed Aug. 28, 2020).\
[2]Microsoft, “Cloud App Security best practices,” Oct. 24, 2019.  
[https://docs.microsoft.com/en-us/cloud-app-security/best-practices](https://docs.microsoft.com/en-us/cloud-app-security/best-practices) (accessed Aug. 28, 2020).\
[3]Microsoft, “Encryption,” Aug. 15, 2019.  
[https://docs.microsoft.com/en-us/microsoft-365/compliance/encryption?view=o365-worldwide](https://docs.microsoft.com/en-us/microsoft-365/compliance/encryption?view=o365-worldwide) (accessed Aug. 28, 2020).\
[4]Microsoft, “Microsoft Defender Advanced Threat Protection,” Microsoft, Aug. 13, 2020.  
[https://docs.microsoft.com/en-us/windows/security/threat-protection/microsoft-defender-atp/microsoft-defender-advanced-threat-protection](https://docs.microsoft.com/en-us/windows/security/threat-protection/microsoft-defender-atp/microsoft-defender-advanced-threat-protection) (accessed Aug. 28, 2020).\
[5]Microsoft, “Office 365 Advanced Threat Protection,” Microsoft, Aug. 12, 2020.  
[https://docs.microsoft.com/en-us/microsoft-365/security/office-365-security/office-365-atp?view=o365-worldwide](https://docs.microsoft.com/en-us/microsoft-365/security/office-365-security/office-365-atp?view=o365-worldwide) (accessed Sep. 02, 2020).\
[6]Microsoft, “Preparing for TLS 1.2 in Office 365 and Office 365 GCC,” Jul. 23, 2020.  
[https://docs.microsoft.com/en-us/microsoft-365/compliance/prepare-tls-1.2-in-office-365?view=o365-worldwide](https://docs.microsoft.com/en-us/microsoft-365/compliance/prepare-tls-1.2-in-office-365?view=o365-worldwide) (accessed Aug. 28, 2020).\
[7]Microsoft, “Service encryption with Customer Key,” Feb. 05, 2020.  
[https://docs.microsoft.com/en-us/microsoft-365/compliance/customer-key-overview?view=o365-worldwide](https://docs.microsoft.com/en-us/microsoft-365/compliance/customer-key-overview?view=o365-worldwide) (accessed Aug. 28, 2020).\
[8]Microsoft, “What is Cloud App Security,” Microsoft, Aug. 15, 2019.  
[https://docs.microsoft.com/en-us/cloud-app-security/what-is-cloud-app-security](https://docs.microsoft.com/en-us/cloud-app-security/what-is-cloud-app-security) (accessed Sep. 02, 2020).\

\newpage
# Uge 35

## Mandag/Tirsdag

Har været dage, hvor jeg fortsat har kigget på DRP’en og Password Manager. Har ikke så meget her, da jeg glemte at skrive ned på dagen, som jeg plejer at gøre, i stedet for sidst på ugen.

## Onsdag

Møde med Kunden om afstemning ang. hvad mødet om torsdagen gik ud på og hvad der var forventet om torsdagen. Resten af dagen gik med at definere tekniske spørgsmål til sikkerhedsvurderingen som fandt sted om torsdagen. Jeg valgte at lave spørgsmålene ud fra CIS kontrollerne og ISO27001/2. Jeg startede med at fokusere på ISO standarderne, men da det har fokus på dokumentation, tog jeg til CIS eftersom de er mere praksis orienteret.

## Torsdag

Sikkerhedsvurderings mødet gik meget godt og CISO’en fra kunden var glad for de spørgsmål jeg stillede. Men da han ikke var med til mødet om Torsdagen, syntes han der var et mangel af spørgsmål vedr. Cloud miljøer. En ting jeg er helt enig I, men det er noget jeg var nødt til at vende tilbage med efter jeg har læst op på det, for at få noget bedre indsigt i cloud sikkerhed. Fandt en companion guide til CIS kontrollerne om cloud, som uddyber de normale kontroller iht. Cloud miljøer.

## Fredag

Fredag er gået med at blive mere bekendt med cloud sikkerhed. Jeg læste op på CIS companion guiden, NIST SP 800-146 og PCI Consortiummets ”Cloud Computing Guidelines”. Heldigvis har det vist sig primært at følde de traditionelle sikkerheds tiltag, dog med lidt software sikkerhed drysset oven i.

Reference dokumenter jeg fandt:\
[PCI Consortium: CLoud Guidelines](https://www.pcisecuritystandards.org/pdfs/PCI_SSC_Cloud_Guidelines_v3.pdf)\
[NIST Special Publication 800-146](https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-146.pdf)\
[CIS Controls Cloud Companion Guide](https://www.cisecurity.org/white-papers/cis-controls-cloud-companion-guide/)\

## Reflektion
Det er fedt at snakke med en ”rigtig” kunde og hører ad hvordan deres IT er udformet. Det har giver mig mål for hvad jeg selv skal kigge efter/læse op på for at kunne være med og blive bedre samt øge mine horisonter. Palo Alto's App-ID system er ret fedt, men det gør at spørgsmål om port management bare bliver "Vi bruger App-ID" :) Men når man sidder ved siden af Lasse som er [PCNSE](https://www.paloaltonetworks.com/services/education/certification) certificeret, så er det nemt nok at få en forklaring på hvordan det virker :)

## Abbendum
Min tilgang til at finde en praktikplads var at gøre brug af de kontakter jeg havde/har rundt omkring (Klasse kammerater fra IT-Teknologen i først omgang) ellers var det at bruge Advisory Board listen eller jer undervisere. Jeg har været meget heldig med kun at have officielt søgt et sted, men havde hørst ad hos de gamle klassekammerater, hvor det ikke lød til at kunne komme på banen.
Det er svært at finde virksomheder, som har det man lige står og leder efter, forstået på den måde at der ikke er nogen lister, man bare kan slå op i.
Og det med at kontakte virksomheder er jeg blevet bedre til end jeg var før hen, takket været de projekter vi lavede i første og andet semester. I forhol til støtte fra UCL og undervisere har jeg ikke haft et behov for støtte under selve praktiksøgningen, men der var støtte op til dette, jf. projekterne fra skoletiden.

\newpage
# Uge 34

## Mandag/Tirsdag/Onsdag

Mandagen gik med at få lavet mit første udkast til en IT-sikkerhedspolitik og fik den sendt til Henrik for at se om han synes jeg har glemt noget. Ellers er de første tre dage gået med at få lavet disaster recovery planen. Der har været meget snak med kollegaer for at få de rigtige informationer, men der er meget arbejde i at få lavet en DRP selv for et lille firma som Kjær-Data.

## Torsdag

Torsdagen er gået med at lave et oplæg til en sikkerhedvurdering af en af KD’s kunder. Det bliver spændende at skulle gøre det. Var også behjælpelig med at finde et program som lavede nogle besynderlige netværks forbindelser, som blev opdaget i Palo Alto firewallen. (Det viste sig at være et stykke FortiNet software, som lavede nogle requests udefra).

## Fredag

Fredagen gik med at holde møde med Henrik om informationer til at stoppe i DRP'en (som jeg endelig er nået til), men mangler stadig en del fra andre dele af firmaet, før jeg er færdig. I forbindelse med DRP'en fik jeg også lavet en risiko-matrix i excel til det risikovurdering, Henrik og jeg har planlagt til næste uge.

\newpage
# Uge 33

## Mandag

Startede med sikkerhedsvurderingsmødet med Henrik, hvor vi blev enige om at der skulle laves en disaster recovery plan samt en IT sikkerhedspolitik. Kollegaerne havde inden mødet efterspurgt en password manager, da kunderne har adgang til de dokumenter, som teknikkerne også har adgang til. Jeg brage også dette op til mødet og Henrik var enig, da det også ville både kryptere adgangskoderne og ville øge password styrken gevaldigt. Resten af dagen gik med at arbejde på DRP'en.

## Tirsdag

Inden for de første par timer af dagen blev jeg færdig med udformingen af DRP'en, men da der stadig er folk på ferie kunne jeg ikke komme videre. Så jeg begyndte at kigge på password managers og spørge kollegaerne om hvad de ville have at den skulle kunne. Jeg skrev selvfølgelig deres ønsker ned og brugte resten af dagen på dette. (Der er alt for mange PW managers!)

## Onsdag
Dagen startede med at jeg havde IT-sikkerhedspolitikken på hovedet, så jeg brugte dagen på at skrive første draft af den. For at det ikke skulle tage alt for lang tid brugte jeg en  IT-sikkerhedspolitik, som Hans havde fundet, så der var lidt inspiration til de punkter, som jeg havde glemt.

## Torsdag
Torsdagen var ikke produktiv i forhold til mine opgaver overhovedet.. Jeg fandt ud af at Palo Alto's Anti-Malware beskyttelse(Ja, det kanlder de det) havde problemer med at connected til serveren, så hele dagen gik med at installere forskellige versioner og bruge cleaner redskaber. Kun for at finde ud af at hvis jeg prøvede at connecte via hotspot fra min mobil virkede det... God social dag!

## Fredag
Fredagen er gået med at arbejde videre på mine opgaver, intet interresant er sket.


\newpage
# Uge 32

## Mandag

Mandagen startede med at få en workstation så jeg kunne komme i gang med at arbejde. Men workstationen blev færdig, havde jeg et møde med Henrik om mulighederne for hvilke opgaver, som vi kunne udarbejde i min tid hos dem. Nogle af disse er at han gerne vil tilføje IT sikkerhed som en af de services Kjær-Data (KD) kan tilbyde i form at konsultation og forøget sikkerhed i deres nuværende services og produkter. Der var også noget skal om at bruge A.I. til at udfører nogle sikkerheds opgaver, med det var ikke mere end idé, som jeg ikke har flere detaljer om end det. Sidste punkt til mødet var min først opgave, som bestod sig i at han godt ville have at jeg skulle lave en sikkerhedsvurdering, på et organisatorisk plan, af KD, som jeg begyndte på efter mødet. Den første del-opgave var at jeg skulle definerer og skrive de spørgsmål ned, som jeg skulle stille Henrik i løbet af ugen.

## Tirsdag 

Tirsdagen startede med at at få defineret og nedskrevet de enkelte spørgsmål, som jeg ikke nåede Mandag. Derefter begyndte jeg at lave en rapport template til svarende og min vudering af KD, så det var klar til når jeg havde haft en spørgerunde med Henrik. Jeg lavede dette, da Henrik ikke var tilgængelig hele dagen.

## Onsdag/Torsdag

Onsdag fik jeg holdt mødet med Henrik, hvilket gav nogle gode indfaldsvinkler for fremtidige spørgerunder og interesante informationer om KD som virksomhed og hvordan det hele er skruet sammen. Resten af Onsdagen og hele Torsdagen gik med at få skrevet svarende og vurderingerne over i rapport dokumentet. 

## Fredag

Det var planen at jeg Fredag skulle holde møde med Henrik, om min vurdering, men han var optaget hele dagen, så mødet bliver holdt Mandag morgen i stedet. Så Fredagen gik primært med socialisering og masser af spørgsmål til kollegaerne om en masse ting ang. KD. (Min vurdering af KD er god, grundet firmaets størrelse. De har bare den samme forglemmelse at skrive ting ned. + en mangel på en DRP er ikke for godt.)